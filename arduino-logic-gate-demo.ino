/*
Logic Gate Demo Program
Version: 1.0.0

Copyright (C) <2015-2020>  <Peter Stevenson> (2E0PGS)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>

#define I2C_ADDR    0x27 // Address of device goes here. You can find it using "I2C Scanner".
#define BACKLIGHT_PIN     3
#define En_pin  2
#define Rw_pin  1
#define Rs_pin  0
#define D4_pin  4
#define D5_pin  5
#define D6_pin  6
#define D7_pin  7

LiquidCrystal_I2C  lcd(I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin);

//----------------Variables-------------------
int Mode = 0;

int btnModeSelect = 4;
int btnInput1 = 3;
int btnInput2 = 2;

int LEDin1 = 8;
int LEDin2 = 7;
int LEDout = 9;

int btnInput1State = 0;
int btnInput2State = 0;
int btnModeSelectState = 0;

bool disableBTN2 = false;
//----------------Variables----------------END

void setup()
{
  pinMode(btnModeSelect, INPUT_PULLUP); //pullup input button
  pinMode(btnInput1, INPUT_PULLUP);
  pinMode(btnInput2, INPUT_PULLUP);

  pinMode(LEDin1, OUTPUT); //LED outputs
  pinMode(LEDin2, OUTPUT);
  pinMode(LEDout, OUTPUT);
  
  lcd.begin (20,4); //set size of LCD to 20 x 4


  lcd.setBacklightPin(BACKLIGHT_PIN,POSITIVE); 
  lcd.setBacklight(HIGH);  

  lcd.print("---Logic Gate Demo--");
  lcd.setCursor (0,1);
  lcd.print("  PROGRAM v1.0.0.0  ");
  lcd.setCursor (0,2);
  lcd.print("Made by: 2E0PGS aka:");
  lcd.setCursor (0,3);
  lcd.print("---Peter Stevenson--");

  digitalWrite(LEDin1, HIGH); //Startup check of LED's
  digitalWrite(LEDin2, HIGH);
  digitalWrite(LEDout, HIGH);

  delay(4000);
  
}

void loop()
{
  btnInput1State = digitalRead(btnInput1);
  if (disableBTN2 == false){
    btnInput2State = digitalRead(btnInput2);    
  }  
  btnModeSelectState = digitalRead(btnModeSelect); 
  
  if (btnModeSelectState == 0){ //Mode changer
    if(Mode < 6){
      Mode++;
      delay(100); //crude debouce 
    }
    if(Mode > 5){ //rollover menu after end item
      Mode = 0;   
      delay(100); 
    }
  }


  if (btnInput1State == 0){
    digitalWrite(LEDin1, HIGH); //LED Lightup on button press    
  }
  else{
    digitalWrite(LEDin1, LOW);    
  }
  if (btnInput2State == 0){
    digitalWrite(LEDin2, HIGH);    
  }
  else{
    digitalWrite(LEDin2, LOW);    
  }

  
  if (Mode == 0){ //----------------------------AND----------------------------
    lcd.setCursor (0,1);
    lcd.print("  Gate Type == AND  ");    
    if (btnInput1State + btnInput2State == 0){      
      lcd.setCursor (0,2);
      lcd.print("     1 + 1 = 1      ");
      digitalWrite(LEDout, HIGH);
    }
    else if (btnInput1State + btnInput2State == 2) {
      lcd.setCursor (0,2);
      lcd.print("     0 + 0 = 0      ");
      digitalWrite(LEDout, LOW);          
    } 
    if (btnInput1State == 0 && btnInput2State == 1) {
      lcd.setCursor (0,2);
      lcd.print("     1 + 0 = 0      ");
      digitalWrite(LEDout, LOW);
    }    
    if (btnInput2State == 0 && btnInput1State == 1) {
      lcd.setCursor (0,2);
      lcd.print("     0 + 1 = 0      ");
      digitalWrite(LEDout, LOW);
    }   
  }
  if (Mode == 1){ //----------------------------OR----------------------------
    lcd.setCursor (0,1);
    lcd.print("  Gate Type == OR  ");    
    if (btnInput1State == 0 && btnInput2State == 1) {
      lcd.setCursor (0,2);
      lcd.print("     1 + 0 = 1      ");
      digitalWrite(LEDout, HIGH);
    }    
    if (btnInput2State == 0 && btnInput1State == 1) {
      lcd.setCursor (0,2);
      lcd.print("     0 + 1 = 1      ");
      digitalWrite(LEDout, HIGH);
    }    
    if (btnInput1State + btnInput2State == 0){      
      lcd.setCursor (0,2);
      lcd.print("     1 + 1 = 1      ");
      digitalWrite(LEDout, HIGH);
    }
    else if(btnInput1State + btnInput2State == 2){
      lcd.setCursor (0,2);
      lcd.print("     0 + 0 = 0      ");
      digitalWrite(LEDout, LOW);
    }
  }
  if (Mode == 2){ //----------------------------NOT----------------------------
    lcd.setCursor (0,1);
    lcd.print("  Gate Type == NOT  ");

    disableBTN2 = true; //disable button 2 as single input gate
       
    if (btnInput1State == 1) {
      lcd.setCursor (0,2);
      lcd.print("       0 = 1      ");
      digitalWrite(LEDout, HIGH);
    } 
    else {
      lcd.setCursor (0,2);
      lcd.print("       1 = 0      ");
      digitalWrite(LEDout, LOW);
    }
  }
  if (Mode == 3){ //----------------------------NAND----------------------------
    lcd.setCursor (0,1);
    lcd.print("  Gate Type == NAND  ");
    
    disableBTN2 = false; //re enable button 2
    
    if (btnInput1State + btnInput2State == 2) {
      lcd.setCursor (0,2);
      lcd.print("     0 + 0 = 1     ");
      digitalWrite(LEDout, HIGH);
    } 
    if (btnInput1State + btnInput2State == 0) {
      lcd.setCursor (0,2);
      lcd.print("     1 + 1 = 0      ");
      digitalWrite(LEDout, LOW);
    }
    if (btnInput1State == 0 && btnInput2State == 1){
      lcd.setCursor (0,2);
      lcd.print("     0 + 1 = 1     ");
      digitalWrite(LEDout, HIGH);
    }
    if (btnInput2State == 0 && btnInput1State == 1){
      lcd.setCursor (0,2);
      lcd.print("     1 + 0 = 1     ");
      digitalWrite(LEDout, HIGH);
    }
  }
  if (Mode == 4){ //----------------------------NOR----------------------------
    lcd.setCursor (0,1);
    lcd.print("  Gate Type == NOR  ");
    if (btnInput1State == 0 && btnInput2State == 1) {
      lcd.setCursor (0,2);
      lcd.print("     1 + 0 = 0      ");
      digitalWrite(LEDout, LOW);
    }    
    if (btnInput2State == 0 && btnInput1State == 1) {
      lcd.setCursor (0,2);
      lcd.print("     0 + 1 = 0      ");
      digitalWrite(LEDout, LOW);
    }    
    if (btnInput1State + btnInput2State == 0){      
      lcd.setCursor (0,2);
      lcd.print("     1 + 1 = 0      ");
      digitalWrite(LEDout, LOW);
    }
    else if(btnInput1State + btnInput2State == 2){
      lcd.setCursor (0,2);
      lcd.print("     0 + 0 = 1      ");
      digitalWrite(LEDout, HIGH);
    }
  }
  if (Mode == 5){ //----------------------------XOR----------------------------
    lcd.setCursor (0,1);
    lcd.print("  Gate Type == XOR  ");
    if (btnInput1State == 0 && btnInput2State == 1) {
      lcd.setCursor (0,2);
      lcd.print("     1 + 0 = 1      ");
      digitalWrite(LEDout, HIGH);
    }    
    if (btnInput2State == 0 && btnInput1State == 1) {
      lcd.setCursor (0,2);
      lcd.print("     0 + 1 = 1      ");
      digitalWrite(LEDout, HIGH);
    }    
    if (btnInput1State + btnInput2State == 0){      
      lcd.setCursor (0,2);
      lcd.print("     1 + 1 = 0      ");
      digitalWrite(LEDout, LOW);
    }
    else if(btnInput1State + btnInput2State == 2){
      lcd.setCursor (0,2);
      lcd.print("     0 + 0 = 0      ");
      digitalWrite(LEDout, LOW);
    }
  }    
}

