# Arduino Logic Gate Demo Program #

### What is this repository for? ###

* Demo of all six Logic Gates using Arduino
* Version 1.0.0.0

### How do I get set up? ###

* Open Code in IDE. Set-up hardware according to schematic.
* Dependencies: Wire.h LCD.h LiquidCrystal_I2C.h

### What does the hardware look like? ###
![imgDemo.jpg](https://bitbucket.org/repo/pqXxR8/images/2564860460-imgDemo.jpg)

### Licence? ###

* http://opensource.org/licenses/GPL-3.0

    Copyright (C) <2015>  <Peter Stevenson>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Who do I talk to? ###

* Repo owner or admin
* 2E0PGS